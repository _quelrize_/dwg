

self.addEventListener('install', function(event) {
    console.log('[Service Worker] Installing Service Worker ...',event);
    event.waitUntil(
      caches.open("static")
        .then(function(cache) {
          console.log("precahing");
          cache.add('/index.html');
          cache.add('/css/bootstrap.css');
          cache.add('/images/logo.png');
          cache.add('/');
        })
    );

});


self.addEventListener('fetch', function(event){
    console.log('[Service Worker] Fetching something ...', event);
  event.respondWith(fetch(event.request));
});

self.addEventListener('fetch', function(event){
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        if (response)
            return response;
        else
            return fetch(event.request);
      }
    )
);
});

self.addEventListener('push', (event) => {
  const notification = event.data.text();
  self.registration.showNotification(notification, {});
});